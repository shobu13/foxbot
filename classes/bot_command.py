from typing import Optional, Union, List, Dict, Any, Callable

import interactions
from interactions import ApplicationCommandType, Guild, Option, MISSING


class Client(interactions.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def command(self, *, type: Optional[Union[int, ApplicationCommandType]] = ApplicationCommandType.CHAT_INPUT,
                name: Optional[str] = MISSING, description: Optional[str] = MISSING,
                scope: Optional[Union[int, Guild, List[int], List[Guild]]] = MISSING, options: Optional[
                Union[Dict[str, Any], List[Dict[str, Any]], Option, List[Option]]
            ] = MISSING, default_permission: Optional[bool] = MISSING) -> Callable[..., Any]:
        command_args = {"type": type, "name": name, "description": description, "scope": scope,
                        "default_permission": default_permission, "options": options}
        client_super = super()

        def wrapper(coro):
            command_args["name"] = command_args["name"] \
                if command_args["name"] != MISSING \
                else str(coro.__name__)
            command_args["description"] = command_args["description"] \
                if command_args["description"] != MISSING \
                else str(coro.__name__)

            @client_super.command(**command_args)
            async def inner(*args, **kwargs):
                await coro(*args, **kwargs)

        return wrapper

    async def wait_until_ready(self) -> None:
        return await super().wait_until_ready()
