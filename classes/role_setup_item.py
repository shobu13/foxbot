from typing import Union

import discord

import settings


class RoleSetupItem():
    def __init__(self, post_id: int, emoji: Union[int, str], role_id: int):
        self.post_id = post_id
        self.emoji = emoji
        self.role_id = role_id

    def validate(self, payload):
        return self.post_id == payload.message_id and (self.emoji == payload.emoji.id if type(
            payload.emoji.id) == int else self.emoji == payload.emoji.name)

    async def add(self, payload):
        user = payload.member
        await user.add_roles(discord.utils.get(user.guild.roles, id=self.role_id),
                             reason="automatic assignement")

    async def remove(self, client, payload):
        guild = await client.fetch_guild(payload.guild_id)
        user = await guild.fetch_member(payload.user_id)
        await user.remove_roles(discord.utils.get(user.guild.roles, id=self.role_id),
                                reason="automatic assignement")
