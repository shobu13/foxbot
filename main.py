import asyncio
from random import choice

import discord
import interactions
import requests as r
from discord.ext import tasks
from sqlalchemy import desc
from inspect import getmembers

import settings
import runners
from models.base import Session
from models.user import User
from settings import bot, dpy
from utils import is_admin


@dpy.event
async def on_ready():
    print('We have logged in as {0.user}'.format(dpy))
    await dpy.change_presence(activity=discord.Game('$help'))


@dpy.event
async def on_ready():
    print('interaction logged')


@dpy.event
async def on_message(message):
    if message.author == dpy.user:
        return
    await dpy.process_commands(message)

    session = Session()
    user = session.query(User).filter(User.discord_id == message.author.id).first() or User(
        name=message.author.nick or message.author.name, discord_id=message.author.id, xp=0)
    user.xp = len(message.content) + user.xp

    session.add(user)
    session.commit()
    session.close()


@dpy.event
async def on_raw_reaction_add(payload):
    # don't exec if user is a bot
    if payload.member == dpy.user:
        return

    # list comprehension to test if the reaction correspond to a role assignation setting
    [await i.add(payload) for i in settings.ROLE_SETUP.values() if i.validate(payload)]


@dpy.event
async def on_raw_reaction_remove(payload):
    # don't exec if user is a bot
    if payload.member == dpy.user:
        return

    # list comprehension to test if the reaction correspond to a role assignation setting
    [await i.remove(dpy, payload) for i in settings.ROLE_SETUP.values() if i.validate(payload)]


@dpy.event
async def on_member_update(before, after):
    if before == dpy.user:
        return
    if before.nick != after.nick:
        session = Session()

        user = session.query(User).filter(User.discord_id == before.id).first()
        user.name = after.nick or after.name
        session.add(user)
        session.commit()
        session.close()


@bot.command(
    name="song"
)
async def song(ctx):
    await ctx.send(choice(settings.COMMUNIST_SONGS))


@bot.command(
    name="level"
)
async def levelCommand(ctx):
    await level(ctx)


@bot.component("level")
async def levelComponent(ctx):
    await level(ctx)


async def level(ctx):
    session = Session()

    user = session.query(User).filter(User.discord_id == int(ctx.author.id)).first()
    session.close()

    if user:
        await ctx.send(f"utilisateur {user.name}: {user.xp} xp", ephemeral=True)


@bot.command(
    name="leaderboard"
)
async def leaderboard(ctx):
    session = Session()

    users = session.query(User).order_by(desc(User.xp))[0:5]
    session.close()

    string = "\n".join([f"> **{i + 1}. {user.name}:** {user.xp} xp" for i, user in enumerate(users)])
    await ctx.send(string, components=interactions.Button(
        style=interactions.ButtonStyle.PRIMARY,
        label="own score",
        custom_id="level"
    ))


@bot.command(
    name="give_xp",
    options=[
        interactions.Option(
            name="user",
            description="User to give xp",
            type=interactions.OptionType.USER,
            required=True,
        ),
        interactions.Option(
            name="amount",
            description="amount of xp to give",
            type=interactions.OptionType.NUMBER,
            required=True,
        ),
    ]
)
@is_admin()
async def give_xp(ctx, user: discord.Member, amount: int):
    session = Session()

    database_user = session.query(User).filter(User.discord_id == user.id).first()

    if database_user:
        database_user.xp += amount
        session.add(database_user)
        session.commit()
    session.close()


@bot.command(
    name="id_emoji",
    options=[
        interactions.Option(
            name="emoji",
            description="what to test",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ]
)
@is_admin()
async def id_emoji(ctx, emoji):
    print(emoji)


@bot.command(
    name="say",
    options=[
        interactions.Option(
            name="message",
            description="What you want to say",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
@is_admin()
async def say(ctx, message):
    await ctx.send(message)


asyncio.new_event_loop()
loop = asyncio.get_event_loop()
task2 = loop.create_task(dpy.start(settings.TOKEN, bot=True))
task1 = loop.create_task(bot._ready())

gathered = asyncio.gather(task1, task2)
loop.run_until_complete(gathered)
