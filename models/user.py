from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import declarative_base

from models.base import Base


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    discord_id = Column(Integer, nullable=False)
    xp = Column(Integer, nullable=False, default=0)
