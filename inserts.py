from sqlalchemy import create_engine

from models.base import Base, engine

from models.user import User

if __name__ == '__main__':
    Base.metadata.create_all(engine)
