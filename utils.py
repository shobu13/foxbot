from discord.ext import commands

import settings


def is_admin():
    async def predicate(ctx):
        return settings.MOD_ROLE_ID in [i.id for i in ctx.author.roles]

    return commands.check(predicate)
